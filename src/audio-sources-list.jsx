import React, { PureComponent, PropTypes } from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';

import AudioSourcesListItem from './audio-sources-list-item';

class AudioSourcesList extends PureComponent {

  static propTypes = {
    styles: PropTypes.object,
    audioSources: ImmutablePropTypes.listOf(
      ImmutablePropTypes.mapContains({
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        size: PropTypes.number.isRequired
      })
    ).isRequired,
    addAudioSource: PropTypes.func.isRequired,
    setAudioSourceName: PropTypes.func.isRequired,
    decodeAudioData: PropTypes.func.isRequired,
    handleSourceItemDropped: PropTypes.func
  };

  static defaultProps = {
    styles: {},
    handleSourceItemDropped: () => {}
  }; 

  state = {
    selectedSourceIdx: null
  };

  handleFileDrag(e) {
    e.preventDefault();
  }

  handleFileDrop(e) {
    const { decodeAudioData, addAudioSource } = this.props;
    const files = e.dataTransfer.files;

    for (let file of files) {
      const reader = new FileReader();

      reader.onload = (e) => {
        const { name, size } = file;
        const arrayBuffer = e.target.result;

        decodeAudioData(arrayBuffer, (buffer) => {
          addAudioSource(name, size, buffer);
        });
      };

      e.preventDefault();
      reader.readAsArrayBuffer(file);
    }
  }

  render() {
    const setState = this.setState.bind(this);
    const handleFileDrag = this.handleFileDrag.bind(this);
    const handleFileDrop = this.handleFileDrop.bind(this);
    const { styles, audioSources, setAudioSourceName, handleSourceItemDropped } = this.props;
    const { selectedSourceIdx } = this.state;

    return (
      <ul
        className={styles['sources-list']}
        onDragOver={handleFileDrag}
        onDrop={handleFileDrop}>
        {audioSources.map((source, i) => {
          return <AudioSourcesListItem
            styles={styles}
            key={i}
            handleSetSelected={(() => { setState({ selectedSourceIdx: i }); })}
            setAudioSourceName={setAudioSourceName}
            handleDropped={handleSourceItemDropped}
            source={source}
            selected={i == selectedSourceIdx} />;
        })}
      </ul>
    );
  }

}

export default AudioSourcesList;