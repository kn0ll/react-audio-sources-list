import React, { PureComponent, PropTypes } from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { DragSource } from 'react-dnd';
import ReactDOM from 'react-dom';
import cx from 'classnames';

const formatName = (str, length = 15, ending = '…') =>
  str.length < length? str:
    `${str.substring(0, length - ending.length)}${ending}`;

const formatSize = (kb) =>
  Math.round((kb / 1024 / 1024) * 10) / 10;

const dragSourceHandlers = {

  beginDrag(props) {
    const { source } = props;

    return { id: source.get('id') };
  },

  endDrag(props, monitor) {
    const item = monitor.getItem();
    const dropResult = monitor.getDropResult();

    if (dropResult) {
      props.handleDropped(dropResult, item.id);
    }
  }

};

const connectDragSourceToProps = (connect, monitor) => ({
  dragConnectDragSource: connect.dragSource(),
  dragIsDragging: monitor.isDragging()
});

@DragSource('audio-source-list-item', dragSourceHandlers, connectDragSourceToProps)
class AudioSourcesListItem extends PureComponent {

  static propTypes = {
    styles: PropTypes.object,
    dragConnectDragSource: PropTypes.func.isRequired,
    dragIsDragging: PropTypes.bool.isRequired,
    handleSetSelected: PropTypes.func.isRequired,
    setAudioSourceName: PropTypes.func.isRequired,
    handleDropped: PropTypes.func.isRequired,
    selected: PropTypes.bool.isRequired,
    source: ImmutablePropTypes.mapContains({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      size: PropTypes.number.isRequired
    }).isRequired
  };

  state = {
    isEditing: false
  };

  handleStartEditing(e) {
    e.preventDefault();
    this.setState({ isEditing: true });
  }

  handleSetName(e) {
    const { source, setAudioSourceName } = this.props;
    const { charCode } = e;
    const id = source.get('id')
    const name = e.target.value;

    if (charCode == 13) {
      name && setAudioSourceName(id, name);
      this.setState({ isEditing: false });
    }
  }

  handleStopEditing(e) {
    this.setState({ isEditing: false });
  }

  componentDidUpdate() {
    const { nameInput } = this.refs;

    if (nameInput) {
      ReactDOM.findDOMNode(nameInput).focus();
    }
  }

  render() {
    const { styles, handleSetSelected, selected, source,
      dragConnectDragSource, dragIsDragging } = this.props;
    const { isEditing } = this.state;

    const handleStartEditing = this.handleStartEditing.bind(this);
    const handleSetName = this.handleSetName.bind(this);
    const handleStopEditing = this.handleStopEditing.bind(this);

    const name = source.get('name');
    const size = source.get('size');

    return (
      dragConnectDragSource(
        <li
          className={cx({
            [styles['selected']]: selected,
            [styles['dragging']]: dragIsDragging 
          })}
          onDoubleClick={handleStartEditing}
          onMouseDown={handleSetSelected}>
          {isEditing?
            <input
              ref="nameInput"
              type="text"
              defaultValue={name}
              onKeyPress={handleSetName}
              onBlur={handleStopEditing} />:
            formatName(name)}<br />
            <span className={styles['sub']}>{formatSize(size)}mb</span>
        </li>
      )
    );
  }

}

export default AudioSourcesListItem;