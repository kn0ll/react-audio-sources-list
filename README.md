# react-audio-sources-list
a React component to manage a list of [redux-audio-sources](https://gitlab.com/kn0ll/redux-audio-sources). while it was designed for redux-audio-sources, you could use it with your own backend as long as you create the appopriate methods the component requires to operate.

[![npm version](https://img.shields.io/npm/v/react-audio-sources-list.svg?style=flat-square)](https://www.npmjs.com/package/react-audio-sources-list)

- [features](#features)
- [installation](#installation)
- [integration](#integration)
- [props](#props)

---

## features

- display list of audioSources
- add an audioSource by dropping a file onto the component
- rename an audioSource by double clicking a list item
- associate an audioSource with other components by dragging and dropping a list item

---

## installation
```
npm install react-audio-sources-list --save
```

---

## integration

### basic use case

require the component and use it in your render function. you must ensure you pass it all required props, discussed later in [props](#props).

```
import React, { PureComponent } from 'react';
import AudioSourcesList from 'react-audio-sources-list';

class MyComponent extends PureComponent {
  
  render() {
    const { audioSources, addAudioSource, setAudioSourceName,
      decodeAudioData } = this.props;
      
    return (
        <AudioSourcesList
          audioSources={audioSources}
          addAudioSource={addAudioSource}
          setAudioSourceName={setAudioSourceName}
          decodeAudioData={decodeAudioData} />
    );
  }

}
```

### supporting drag and drop

the component can accept prop `handleSourceItemDropped` which will be called whenever a list item is dropped onto a valid [react-dnd](https://github.com/gaearon/react-dnd) `DropTarget`. your callback will be passed the audioSource `id` and whatever your DropTarget's `drop` method returns.

```
import React, { PureComponent } from 'react';
import { DropTarget } from 'react-dnd';
import AudioSourcesList from 'react-audio-sources-list';

@DropTarget('audio-source-list-item', {
  drop(props) {
    return { name: props.name };
  }
}, (connect, monitor) => ({
  dragConnectDropTarget: connect.dropTarget()
}))
class MyDropZone extends PureComponent {
  
  render() {
    const { dragConnectDropTarget, name } = this.props;

    return (
      dragConnectDropTarget(
        <div>drop audioSource list item into {name}</div>
      )
    );
  }

}

class MyComponent extends PureComponent {
  
  render() {
    const { audioSources, addAudioSource, setAudioSourceName,
      decodeAudioData } = this.props;

    return (
      <div>
        <AudioSourcesList
          audioSources={audioSources}
          addAudioSource={addAudioSource}
          setAudioSourceName={setAudioSourceName}
          decodeAudioData={decodeAudioData}
          handleSourceItemDropped={(dropResult, audioSourceId) => {
            alert(`audioSource ${audioSourceId} was dropped into
              ${dropResult.name}`);
          }} />
        <MyDropZone
          name="test drop zone" />
        <MyDropZone
          name="another test drop zone" />
      </div>
    );
  }

}
```

### styling the component

the component can be styled by [CSS Modules](https://github.com/css-modules/css-modules). simply pass the component a `styles` prop which contains a CSS Module. this package also includes a [default CSS Module](https://gitlab.com/kn0ll/react-audio-sources-list/blob/master/src/audio-sources-list.scss) you can use.

```
import React, { PureComponent } from 'react';
import AudioSourcesList from 'react-audio-sources-list';
import audioSourcesListStyles from 'react-audio-sources-list/lib/styles.css';

class MyComponent extends PureComponent {
  
  render() {
    const { audioSources, addAudioSource, setAudioSourceName,
      decodeAudioData } = this.props;
      
    return (
        <AudioSourcesList
          styles={audioSourcesListStyles}
          audioSources={audioSources}
          addAudioSource={addAudioSource}
          setAudioSourceName={setAudioSourceName}
          decodeAudioData={decodeAudioData} />
    );
  }

}
```

---

## props

- `audioSources` - an Immutable list of audioSource. provided by redux-audio-sources.
- `addAudioSource` - a method for adding an audioSource to audioSources. provided by redux-audio-sources.
- `setAudioSourceName` - a method for changing an audioSource name. provided by redux-audio-sources.
- `decodeAudioData` - a method that can turn an ArrayBuffer into an AudioBuffer. one is provided by your AudioContext: `audioContext.decodeAudioData.bind(audioContext)`
- `handleSourceItemDropped` (optional) - a callback method to handle when an audioSource list item is dropped into a valid react-dnd DropZone.
- `styles` (optional) - a CSS module to style the component.