var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {

  output: {
    libraryTarget: 'commonjs2',
    path: path.join(__dirname, 'lib'),
  },

  resolve: {
    extensions: ['', '.js', '.jsx']
  },

  plugins: [
    new ExtractTextPlugin('styles.css')
  ],

  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loader: 'babel'
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract(
          'css?importLoaders=1&!sass'
        )
      }
    ],
  },

};